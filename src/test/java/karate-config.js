function karateConfig() {
    var env = karate.env; // get system property 'karate.env'
    karate.log('karate.env system property was:', env);
    karate.configure('logPrettyResponse', true);
    if (!env) {
        env = 'dev';
    }
    var config = {
        env: env,
        baseUrl: 'https://reqres.in/api/'
    }

    if (env == 'dev') {
        config.baseUrl = 'https://reqres.in/api/';

    }

    else if (env == 'qa') {
        config.baseUrl = 'https://reqres.in/api/';
    }

    else if (env == 'prod') {
        config.baseUrl = 'https://reqres.in/api/';
    }

    var result = karate.callSingle('classpath:endpoints/headers/headers.feature', config);

    return config;
}