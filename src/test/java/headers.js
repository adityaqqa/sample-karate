function fn() {
  var token = karate.get('token');
  if (token) {
    return {
        Authorization: token
    };
  } else {
    return {};
  }
}