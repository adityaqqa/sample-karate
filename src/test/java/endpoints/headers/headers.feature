@ignore
Feature: headers
# this feature file will not run as part of the tests and will only run before runninf all the tests to get the authorization code

  Background:
    * url baseUrl
    * def username = "eve.holt@reqres.in"
    * def password = "cityslicka"

  Scenario: Verify sample get api
    Given path 'login'
    Given request { email: '#(username)', password: '#(password)' }
    When method post
    Then status 200
    * def token = response.token