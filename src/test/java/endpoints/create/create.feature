Feature: create

  Background:
    * url baseUrl
    * def name = "morpheus"
    * def job = "leader"
    * def job_updated = "zion president"
    * configure headers = read('classpath:headers.js')

  Scenario: Create and update a user
    * print '### Create'
    Given path 'users'
    Given request { name: '#(name)', job: '#(job)' }
    When method post
    Then status 201
    And match response contains {"name": '#(name)'}
    And def created_user = response

    * print '### Update'
    Given path 'users', created_user.id
    Given request { name: '#(name)', job: '#(job_updated)' }
    When method put
    Then status 200
    And match response contains {"job": '#(job_updated)'}
