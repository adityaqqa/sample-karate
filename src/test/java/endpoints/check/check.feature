Feature: check

  Background:
    * url baseUrl
    * def pageNo = 2
    * configure headers = read('classpath:headers.js')

  Scenario: Verify sample get api
    Given path 'users'
    Given param page = pageNo
    When method get
    Then status 200
    Then match response contains {"page": '#(pageNo)'}